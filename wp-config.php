<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'SmavlwKfuXfp6x4s+Wv/LnqIpzyXuKaldeMNIpHgtE9Q1thcl2Ra7+E0r5vjGeKJ9oqmh8Z2DP3U7bLk2tCmFg==');
define('SECURE_AUTH_KEY',  '3rAgSogWHhcpBuvX61ahDA3GjFuAuySm7R7AtYAD9D0ZexZkLHzI5+y8OP1p1qJ5/Kxb8m7OMg+kycjyn97aMQ==');
define('LOGGED_IN_KEY',    'OvcBXT1DMWaw+1pO0fP0rsAKiuRvt6pYGIn7LFEk/Xs5fXF8QHtfAYxDnfY95GipdR+9dW8a7xLoNCA6KMls/g==');
define('NONCE_KEY',        'DGRVnyhW/8Ketceb5EGVl9K+1WeoYNP23KVVA5cqdcFlyGhxkzDoSqGHXxfVptRi81yRUj74iUJ11WzU56O4pg==');
define('AUTH_SALT',        'O6nfD7gi7jKOeElhM4TaRJGe8fWpzwE/tN9IjAmtk9lMWmM1e4QFJ7BtDkyZiUJAeunyghpscyiuHdy0TKM82Q==');
define('SECURE_AUTH_SALT', 'PU3SPMIU/Ltlz/JKmt3Z5stnSeuQ4SwZcs/IWltuLDR11ICUOhcYxIr7DohRtYjSgDts6r44IaqxMFL3H93zQg==');
define('LOGGED_IN_SALT',   'JSR7gusdD0j8GPyrHf9nxrFh0L9bKs9hZxrCJXZyJlLWCp+nElZx8wZVrv+k8266GApNq0JrSpyk4p5QiPa/zg==');
define('NONCE_SALT',       'j+2vdEg9gTV2zuIaeqO8XwdTIw6uywQqyAk+xOsNgjW3t5EvwDq4Bu11XL7FvN9bo2824JVCxmjnAPEtyBU5Ew==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
