<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Título do site</title>
    <link rel="icon" href="assets/img/logo-in-junior.png">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/reset.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">
    <?php wp_head() ?>
</head>
<body>
    <header>
        <nav class="container">
            <a href="<?php if (is_front_page()){echo "javascript:void(0)";}else{echo get_home_url();}?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logoIN.svg" alt="logo"></a>
            <input type="checkbox" id="checkconfig" style="display: none"/>
            <label for="checkconfig" class="nav-icon">
                <div></div>
            </label>

            <?php
                $args = array(
                    'menu' => 'navBar',
                    'container' => true
                );

                wp_nav_menu($args);
            ?>
<!--
            <ul id="menu-navegacao" class="menu">
                <li ><a href="servicos">Serviços</a></li>
                <li ><a href="noticias">Notícias</a></li>
                <li ><a href="contato">Contato</a></li>
            </ul>-->
        </nav>
    </header>