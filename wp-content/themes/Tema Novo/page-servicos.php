<?php 
    // Template Name: Serviços

    get_header();
?>

<main id="servicos">
    <h1>Serviços</h1>
    <section class="container">
        <div class="card">
            <div><h2>Foco</h2></div>
            <ul>
                <li>Paranauê</li>
                <li>Paranauê</li>
                <li>Paraná</li>
            </ul>
        </div>
        <div class="card">
            <div><h2>Força</h2></div>
            <ul>
                <li>Pipipipopopo</li>
                <li>Pipipipopopo</li>
                <li>Pipipipopopo</li>
            </ul>
        </div>
        <div class="card">
            <div><h2>Fé</h2></div>
            <ul>
                <li>pára pá pá</li>
                <li>pá pá pá</li>
                <li>POOOOOWWW</li>
            </ul>
        </div>
    </section>
</main>

<?php
    get_footer();
?>
